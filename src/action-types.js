import { createPrefixedActionToken } from './util';

const authActionsPrefix = 'Auth';
const actionToken = createPrefixedActionToken(authActionsPrefix);

const authActions = {
  AUTH_ACTIONS_PREFIX: authActionsPrefix,
  LOGIN_LOADING: actionToken('LOGIN_LOADING'),
  LOGIN_SUCCESS: actionToken('LOGIN_SUCCESS'),
  LOGIN_ERROR: actionToken('LOGIN_ERROR'),
  LOGOUT: actionToken('LOGOUT'),
  SET_USER: actionToken('SET_USER'),
  LOAD_USER: actionToken('LOAD_USER'),
  LOAD_USER_FAILED: actionToken('LOAD_USER_FAILED'),
  SET_AUTH_TOKEN: actionToken('SET_AUTH_TOKEN'),
  SET_USER_ID: actionToken('SET_USER_ID')
};

export default {
  ...authActions
}