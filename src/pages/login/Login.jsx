import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { loginUser } from '../../reducers/auth'

export const FORM_NAME_TOKEN = 'login-form';

const FormField = ({ input, label, type, meta: { touched, error } }) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type} />
      {touched && error && <span>{error}</span>}
    </div>
  </div>
);

const Form = (props) => {
  const onSubmit = props.handleSubmit(props.onSubmit);
  return (
    <form onSubmit={onSubmit}>
      <div>
        <Field
          name="email"
          label="Email"
          component={FormField}
          type="email" />
      </div>
      <div>
        <Field
          name="password"
          label="Password"
          component={FormField}
          type="password" />
      </div>
      <button type="submit">Submit</button>
    </form>
  )
};

const LoginForm = reduxForm({ form: FORM_NAME_TOKEN })(Form);

const mapDispatchToProps = dispatch => ({
  onSubmit(value) {
    dispatch(loginUser(value))
  }
})

class LoginPage extends React.Component {
  render() {
    return (
      <div>
        <h1>Login</h1>

        <LoginForm onSubmit={this.props.onSubmit} />
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(LoginPage);