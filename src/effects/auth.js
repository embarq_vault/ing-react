import agent from '../agent';
import actionTypes from '../action-types';
import { setAuthTokenAction, loadUser, setUserIdAction } from '../reducers/auth';

export const authEffectsMiddleware = store => next => action => {
  if (!action.type.includes(actionTypes.AUTH_ACTIONS_PREFIX)) {
    return next(action);
  }

  switch (action.type) {
    case actionTypes.SET_AUTH_TOKEN:
      localStorage.setItem('jwt', action.payload);
      agent.auth.setToken(action.payload);
      break;
    case actionTypes.LOGIN_SUCCESS:
      store.dispatch(setAuthTokenAction(action.payload.id));
      store.dispatch(setUserIdAction(action.payload.userId));
      break;
    case actionTypes.LOGOUT:
      localStorage.removeItem('jtw');
      localStorage.removeItem('uid');
      break;
    case actionTypes.SET_USER_ID:
      localStorage.setItem('uid', action.payload);
      store.dispatch(loadUser(action.payload));
      break;
    default:
      return next(action);
  }

  return next(action);
}