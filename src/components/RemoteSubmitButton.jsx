import React from 'react'
import { connect } from 'react-redux'
import { submit } from 'redux-form';

const RemoteSubmitButton = ({ dispatch, formName, label }) => (
  <button
    type="button"
    onClick={() => dispatch(submit(formName))}>
    {label}
  </button>
);

export default connect()(RemoteSubmitButton);