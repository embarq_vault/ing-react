/** @internal */
export const createPrefixedActionToken = (prefix) => (actionToken) => `[${ prefix }] ${actionToken}`;