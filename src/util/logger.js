import { createLogger } from 'redux-logger';

function loggerPredicate(_, action) {
  try {
    return !action.type.includes('@@redux-form') && !Array.isArray(action);
  } catch (error) {
    return true;
  }
}

export default createLogger({
  collapsed: true,
  predicate: loggerPredicate
})