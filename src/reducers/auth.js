import { store } from '../store';
import { bind } from 'redux-effects';
import { createAction, handleActions } from 'redux-actions';
import actionTypes from '../action-types';
import agent from '../agent';

export const loginLoadingAction = createAction(actionTypes.LOGIN_LOADING);
export const loginSuccessAction = createAction(actionTypes.LOGIN_SUCCESS);
export const loginErrorAction = createAction(actionTypes.LOGIN_ERROR);
export const setAuthTokenAction = createAction(actionTypes.SET_AUTH_TOKEN);
export const logoutAction = createAction(actionTypes.LOGOUT);
export const setUserAction = createAction(actionTypes.SET_USER);
export const loadUserAction = createAction(actionTypes.LOAD_USER);
export const loadUserFailedAction = createAction(actionTypes.LOAD_USER_FAILED);
export const setUserIdAction = createAction(actionTypes.SET_USER_ID);

export const loginUser = credentials => [
  loginLoadingAction(),
  bind(
    agent.auth.login(credentials),
    ({ value }) => loginSuccessAction(value),
    ({ value }) => loginErrorAction(value)
  )
];

export const loadUser = userId => [
  loadUserAction(),
  bind(
    agent.auth.getUser(userId),
    ({ value }) => setUserAction(value),
    ({ value }) => loadUserFailedAction(value)
  )
];

export const updateAuthToken = value => store.dispatch(setAuthTokenAction(value));
export const updateUserId = id => store.dispatch(setUserIdAction(id));

const initialState = {
  login: {
    loading: false,
    success: null,
    error: null
  },
  user: {
    loading: false,
    error: null
  }
}

const reducer = handleActions(
  {
    [loginLoadingAction]: state => ({
      ...state,
      login: {
        loading: true,
        success: null,
        error: null
      }
    }),
    [loginSuccessAction]: (state, action) => {
      return ({
        ...state,
        login: {
          loading: false,
          success: true,
          error: null
        },
        userId: action.payload.userId
      });
    },
    [loginErrorAction]: (state, action) => {
      return ({
        ...state,
        login: {
          loading: false,
          success: false,
          error: action.payload.error
        }
      });
    },
    [loadUserAction]: (state) => ({
      ...state,
      user: {
        ...state.user,
        error: null,
        loading: true
      }
    }),
    [loadUserFailedAction]: (state, action) => ({
      ...state,
      user: {
        ...state.user,
        loading: false,
        error: action.payload.error
      }
    }),
    [setUserAction]: (state, action) => ({
      ...state,
      user: {
        ...state.user,
        ...action.payload,
        loading: false,
        error: null
      }
    }),
    [setUserIdAction]: (state, action) => ({
      ...state,
      user: {
        ...state.user,
        id: action.payload
      }
    })
  },
  initialState
);

export default reducer;