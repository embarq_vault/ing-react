import React from 'react';
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
import Home from './pages/home/Home';
import Login from './pages/login/Login';
import './App.css';

function Router({ children }) {
  return (
    <BrowserRouter>
      <Link to="/">Home</Link>
      <Link to="/login">Login</Link>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default () => (
  <Router></Router>
);
