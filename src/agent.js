import { fetch } from 'redux-effects-fetch';
import { updateAuthToken, updateUserId } from './reducers/auth';

export class AgentService {
  /** @param {string} apiUrl */
  constructor(apiUrl) {
    this.token = null;
    this.apiUrl = apiUrl;

    /** @type {RequestInit} */
    const getFetchOptions = (method, body) => {
      const authorizationHeader = this.token ? {'Authorization': this.token} : {};
      return {
        method,
        body: JSON.stringify(body),
        headers: {
          'Content-Type': 'application/json',
          ...authorizationHeader
        }
      }
    }

    /** @param {string} endpoint */
    const appendOrigin = endpoint => `${this.apiUrl}/${endpoint}`;
    const fetchFactory = method => (url, body) => fetch(
      appendOrigin(url),
      getFetchOptions(method, body)
    );

    this.get = fetchFactory('GET');
    this.del = fetchFactory('DELETE');
    this.put = fetchFactory('PUT');
    this.patch = fetchFactory('PATCH');
    this.post = fetchFactory('POST');
  }

  setToken(token) {
    this.token = token;
  }
}

const agent = new AgentService('https://ing-invoicing.herokuapp.com/api');

const auth = {
  getCurrentUser: () => agent.get('ing-users/'),
  login: (credentials) => agent.post('ing-users/login', credentials),
  setToken: (jwt) => agent.setToken(jwt),
  maybeHydrateToken: () => {
    const token = localStorage.getItem('jwt');
    if (token != null) {
      updateAuthToken(token);
    }
  },
  maybeHydrateUserId: () => {
    const uid = localStorage.getItem('uid');

    if (uid != null) {
      updateUserId(uid);
    }
  },
  isAuthenticated: () => {
    return localStorage.hasOwnProperty('jwt') && localStorage.getItem('jwt') != null;
  },
  getUser: (uid) => agent.get(`ing-users/${ uid }`)
}

export default {
  auth
};