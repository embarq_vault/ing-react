import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import multiMiddleware from 'redux-multi';
import effectsMiddleware from 'redux-effects';
import fetch, { fetchEncodeJSON } from 'redux-effects-fetch';
import { composeWithDevTools } from 'redux-devtools-extension';
import { authEffectsMiddleware } from './effects/auth';
import loggerMiddleware from './util/logger';

import reducer from './reducer';

const middlewares = [
  loggerMiddleware,
  thunkMiddleware,
  multiMiddleware,
  effectsMiddleware,
  fetch,
  fetchEncodeJSON,
  authEffectsMiddleware
];

export const store = createStore(
  reducer,
  composeWithDevTools(
    applyMiddleware(...middlewares)
  )
);